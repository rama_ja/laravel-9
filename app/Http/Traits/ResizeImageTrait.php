<?php

namespace App\Http\Traits;

use Intervention\Image\Facades\Image;

trait ResizeImageTrait
{
    public function resizeImagee($file, $folder) {
        $input['file'] = time().'.'.$file->getClientOriginalExtension();
        // Resize image
        $imgFile = Image::make($file->getRealPath());
        $imgFile->resize(600, 350, function ($constraint) {
            $constraint->aspectRatio();
        })->save($folder.'/'.$input['file']);

        $image_name_DataBase = $folder . '/' . $input['file'];

        return $image_name_DataBase;
    }
}
