<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\StoreUserRequest;
use App\Http\Requests\Api\User\UpdatePasswordRequest;
use App\Http\Requests\Api\User\UpdateUserRequest;
use App\Http\Resources\Api\User\UserResource;
use App\Http\Traits\ApiResponseTrait;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use ApiResponseTrait;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required',
            'password'  => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (!$token = auth('api')->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }

    public function register(StoreUserRequest $request)
    {
        $user = User::create([
            'name'              => $request->name,
            'email'             => $request->email,
            'password'          => Hash::make($request->password),
        ]);
        $token = auth('api')->login($user);
        return response()->json([
            'message'      => 'User successfully registered',
            'access_token' => $token,
            'user'    => new UserResource($user)
        ], 201);
    }

    public function updateInfo(UpdateUserRequest $request)
    {
        $user = $request->user('api');


        $user->update([
            'name'              => $request->name,
            'email'             => $request->email,
        ]);

        return $this->apiResponse(new UserResource($user), 'The user has been updated successfully', 200);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user = $request->user('api');
        if (!auth('api')->attempt(['email'=>$user->email,'password'=>$request->old_password])) {
            return $this->apiResponse(null, 'The old password is incorrect', 200);
        }
        else{
            $user->update([
                'password'          => Hash::make($request->password)
            ]);

            return $this->apiResponse(new UserResource($user), 'The password has been updated successfully', 200);
        }


    }
    public function logout(Request $request)
    {
        if ($request->token) {
            $tokens = ClientDeviceToken::where('token', $request->token)->first();
            $tokens->delete();
        }
        auth('api')->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }
    protected function createNewToken($token)
    {
        return response()->json([
            'access_token'  => $token,
            'token_type'    => 'bearer',
            'expires_in'    => auth('api')->factory()->getTTL() * 60,
            'user'     => new UserResource(auth('api')->user())
        ]);
    }
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
